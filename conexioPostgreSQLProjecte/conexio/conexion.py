import os
import psycopg2
from psycopg2 import OperationalError


class Connection(object):
    def __init__(self):
        try:
            self._host = os.environ['PSQL_HOST']
            self._port = os.environ['PSQL_PORT']
            self._user = os.environ['PSQL_USER']
            self._dbname = os.environ['PSQL_DBNAME']
            self._password = os.environ['PSQL_PASSWORD']
            self._sslmode = os.environ['PSQL_SSLMODE']

            conn_string = f"host={self._host} port={self._port} user={self._user} " \
                          f"dbname={self._dbname} password={self._password} " \
                          f"sslmode={self._sslmode}"
            self.conn = psycopg2.connect(conn_string)
        except OperationalError:
            print("Error al moment de conectarse")


    """
    Tanca la connexió a la base de dades
    """
    def close(self):
        try:
            if self.conn:
                self.conn.close()
        except OperationalError:
            print("No es posssible conectar-se a la base de dades")

    def commit(self):
        self.conn.commit()

    def rooback(self):
        self.conn.rollback()

    def cursor(self):
        self.conn.cursor()

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    @property
    def user(self):
        return self._user

    @property
    def dbname(self):
        return self._dbname

    @property
    def password(self):
        return self._password

    @property
    def sslmode(self):
        return self._sslmode

    @property
    def conn(self):
        return self._conn
