import sqlite3

_DEFAULT_EXT = "db"


def connect_to_db(db_name=None):
    if db_name is None:
        bbdd = ":memory:"
    else:
        bbdd = f"{db_name}.{_DEFAULT_EXT}"
    return sqlite3.connect(bbdd)


def create_table(conn: sqlite3.Connection):
    sql = "CREATE TABLE persona (" \
          "id INTEGER PRIMARY KEY AUTOINCREMENT," \
          "nom TEXT," \
          "cognom TEXT," \
          "edat INTEGER," \
          "email TEXT UNIQUE);"
    try:
        conn.execute(sql)
    except sqlite3.OperationalError as e:
        print(e)


def disconnect_from_db(conn):
    if conn is not None:
        conn.close()


def insert_one(conn, nom, cognom, edat, email):
    sql = "INSERT INTO persona(nom, cognom, edat, email) VALUES(?,?,?,?)"
    try:
        conn.execute(sql, (nom, cognom, edat, email))
        conn.commit()
        print("insertat correctament..!!")
    except sqlite3.IntegrityError as e:
        print(e)


def insert_many(conn: sqlite3.Connection, items: list):
    sql = "INSERT INTO persona(nom, cognom, edat, email) " \
          "VALUES(?,?,?,?)"
    elements = list()
    for x in items:
        elements.append((x['nom'], x['cognom'], x['edat'], x['email']))
    try:
        conn.executemany(sql, elements)
        conn.commit()
        print("insertat correctament..!!")
    except sqlite3.IntegrityError as e:
        print(e)


def select_all(conn):
    cur = conn.execute("SELECT * FROM persona;")
    return cur.fetchall()

def select_by_email(conn: sqlite3.Connection,valor):
    sql=f"SELECT * FROM persona WHERE email='{valor}';"
    cur = conn.execute(sql)
    return cur.fetchone()

def select_by_pk(conn: sqlite3.Connection,valor):
    sql=f"SELECT * FROM persona WHERE id='{valor}';"
    cur = conn.execute(sql)
    return cur.fetchone()