from db.connection import connect_to_db, insert_many, select_all, insert_one, create_table, disconnect_from_db


def mock_items():
    return [{"nom": "Julia", "cognom": "fernandes", "edat": 27, "email": "julia@gmail.com"},
            {"nom": "jose", "cognom": "vicente", "edat": 54, "email": "jose@gmail.com"},
            {"nom": "pepe", "cognom": "nadal", "edat": 26, "email": "pepe@gmail.com"},
            {"nom": "felix", "cognom": "marti", "edat": 21, "email": "felix@gmail.com"}
            ]


def main():
    conn = connect_to_db("basedades")
    #create_table(conn)
    #insert_one(conn, "junior", "salinas", 25, "juniorsd22@gmail.com")
    insert_many(conn, mock_items())
    persones = select_all(conn)
    print(persones[0])
    disconnect_from_db(conn)


if __name__ == '__main__':
    main()
