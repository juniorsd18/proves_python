import os

import psycopg2


class Connection(object):
    def __init__(self):
        self._host = os.environ['PSQL_HOST']
        self._port = os.environ['PSQL_PORT']
        self._user = os.environ['PSQL_USER']
        self._dbname = os.environ['PSQL_DBNAME']
        self._password = os.environ['PSQL_PASSWORD']
        self._sslmode = os.environ['PSQL_SSLMODE']
        conn_string = f'host={self._host} ' \
                      f'port={self._port} ' \
                      f'user={self._user} ' \
                      f'dbname={self._dbname} ' \
                      f'password={self._password} ' \
                      f'sslmode={self._sslmode} '
        self._conn = psycopg2.connect(conn_string)

    def close(self):
        self._conn.close()

    def cursor(self):
        return self._conn.cursor()

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, host):
        self._host = host

    @property
    def port(self):
        return self.port

    @port.setter
    def port(self, port):
        self.port = port

    @property
    def user(self):
        return self.user

    @user.setter
    def user(self, user):
        self.user = user

    @property
    def dbname(self):
        return self.dbname

    @dbname.setter
    def dbname(self, dbname):
        self.dbname = dbname

    @property
    def password(self):
        return self.password

    @password.setter
    def password(self, password):
        self.password = password

    @property
    def sslmode(self):
        return self.sslmode

    @sslmode.setter
    def sslmode(self, sslmode):
        self.sslmode = sslmode


    @property
    def conn(self):
        return self._conn
