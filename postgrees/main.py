from db.connections.connection import Connection


def run_main():
    conn = Connection()
    sql = 'SELECT * FROM usuari ORDER BY id_persona DESC;'

    cursor = conn.cursor()
    cursor.execute(sql)
    result = cursor.fetchall()
    print(result)


if __name__ == '__main__':
    run_main()
